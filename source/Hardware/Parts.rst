Parts list
==========

Electronic components
---------------------
The following table lists all electronic components, that I used to set up Lepta. Some of these components are optional, such as the LED ring. I  ordered the parts at `Mouser <https://mouser.com/>`_ due to price and availability, however other suppliers may serve you as well.

.. list-table:: Electronic components
   :widths: 10 50 30
   :header-rows: 1
   :align: center

   * - Qty.
     - Part name
     - Description
   * - 1
     - Raspberry Pi 4
     -
   * - 1
     - LiPo 3S
     - Battery
   * - 1
     - WS2812B LED ring
     - 24 RGB LEDs
   * - 1
     - 40mm PWM Fan
     -
   * - 1
     - Adafruit INA260
     - Voltage/Current meter
   * - 18
     - Dynamixel AX-12A
     - Servo
   * - 1
     - Dynamixel U2D2
     - Servo Interface
   * - 1
     - ROBOTIS Robot Cable-3P 140mm 10 pcs
     - Cable
   * - 1
     - ROBOTIS Robot Cable-3P 180mm 10 pcs
     - Cable
   * - 1
     - ROBOTIS Robot Cable-3P 200mm 10 pcs
     - Cable
   * - 1
     - ROBOTIS P04-F2 10pcs
     -
   * - 1
     - ROBOTIS P04-F3 10pcs
     -
   * - 2
     - ROBOTIS BPF-WA/BU 10pcs
     -
   * - 10
     - MOLEX 22-03-5035
     - PCB Connectors

.. warning::
  The AX-12A Bulk packs (6 Pack) ships without the P04-F2/F3 or BPF-WA/BU.
  If you order the AX-12A in single pack they ship with P04-F2, P04-F3, BPF-WA/BU and screws each.
  I recommend to order the 6 pack as listed in the table and oder the P04-F2, P04-F3, BPF-WA/BU separately. This leaves no spare parts and additional screws at the end of the build.

Body Parts
----------

The bodywork of Lepta consists of a set of custom 3D-printed parts. Personally, I printed all parts on a Prusa MK3S in PETG with no supports requiered.
All part files can be found at `Prusa Printers. <https://www.prusaprinters.org/prints/71601-lepta-v10>`_

The table below lists all parts with the color, that I chose for printing.

.. list-table:: Custom body parts
   :widths: 10 50 30
   :header-rows: 1
   :align: center

   * - Qty.
     - Description
     - Color
   * - 1
     - Lower Body
     - black
   * - 1
     - Lower Body top
     - black
   * - 1
     - Upper Body
     - black
   * - 6
     - Femur (2 parts)
     - orange
   * - 6
     - Tibia
     - orange
   * - 1
     - Ring Bar
     - black
   * - 1
     - Ring Bar Spacer
     - black
   * - 1
     - Ring Cover
     - white

Note that the Ring Cover should be printed in a light, colorless material to allows light from the LED ring to pass through.



Screws
------

Lepta is held together my metric M2 and M3 machine screws. These screws distribute over the 6 legs and the body.
The table below lists the amount of screws needed for each leg as well as the body and the total quantity necessary for the assembly.

.. list-table:: Machine screws
   :widths: 50 15 15 15
   :header-rows: 2
   :align: center

   * -
     - per leg
     - body
     - total
   * -
     - Qty.
     - Qty.
     - Qty.
   * - M2 x 6 mm
     - 36
     - 56
     - 272
   * - M2 x 10 mm
     - --
     - 4
     - 4
   * - M2 x 12 mm / 16 mm
     - --
     - 4
     - 4
   * - M3 x 10 mm
     - 3
     - --
     - 18
   * - M2 Nut
     - 16
     - 48
     - 144

Machine screws can usually be sourced at your local hardware store or on the internet.
