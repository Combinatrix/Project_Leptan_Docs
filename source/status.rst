Status of the Project
=====================

Current Capabilities
--------------------


Lepta can walk straight, turn, walk in circles or move its body without moving the feet.
The robot is controlled using a Bluetooth joy pad, as time of writing only the XBOX One joy pad has been configured.

Lepta can perform the following actions as of time of writing:

- Stand up/Sit down
- Rotate the body without moving the feet (prove of inverse kinematics)
- Walking straight in arbitrary direction with arbitrary speed
- Turning around arbitrary point with arbitrary speed


Planned Features
----------------

These are just a few things I intend to implement in the future

Hardware
~~~~~~~~

- Removable Battery (Make it easier)
- Relay to turn off the Servos completely
- Smaller DC/DC converter
- Sensors
  - LIDAR
  - IMU
  - Camera
- ...

Software
~~~~~~~~

- Smooth leg motion
- Adjustable leg step height
- Further optimize gait
- Different gait styles
- ...
