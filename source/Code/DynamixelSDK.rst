Dynamixel SDK
=============

The `Dynamixel SDK <https://emanual.robotis.com/docs/en/software/dynamixel/dynamixel_sdk/overview/>`_ is an external repo provided by Dynamixel.

.. epigraph::

  The ROBOTIS Dynamixel SDK is a software development kit that provides Dynamixel control functions using packet communication. The API is designed for Dynamixel actuators and Dynamixel-based platforms. For more information on Dynamixel SDK, please refer to the e-manual below.

  -- ROBOTIS Dynamixel SDK

.. todo : Vielleicht kannst du die Blockquote von Dynamixel noch etwas besser hervorheben. I.e. so das klar ist, dass das von der Dynamixel-Seite zitiert ist.
