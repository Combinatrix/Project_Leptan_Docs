.. _InstallationGuide:

Installation
============

Leptan can be installed in two ways. (Check comment in code.)

..
  Das hier ist wirklich nur ein Vorschlag. Ziel war es diese Seite mit ein wenig information zu füllen und eine schlüssige Einleitung zur Installation zu erstellen.
  Falls du eine super-kurze Übersicht bevorzugst, dann belass es dabei.

While the first way is a mere visualization of Lepta, it provides insights into Lepta's motion and technology without the requirement of hardware being available. This may help you to decide whether you wish to build Lepta on your own.

The second method requires the assembled hardware of Lepta. I recommend to move through the hardware and assembly completely, before starting the installations.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Installation/PC_setup
   Installation/RPi_setup
