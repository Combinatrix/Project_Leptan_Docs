Welcome to Project Leptan's Documentation!
===============================================

Project Leptan is a fully open source, 3D printed hexapod robot, based on the open source `Robot Operating System ROS2 <https://www.ros.org/>`_.


+-----------------------------------+-----------------------------------------+
| .. image:: Pictures/Lepta_01.jpg  | .. image:: Pictures/Lepta_walking_1.gif |
+-----------------------------------+-----------------------------------------+

I started the project to learn some more about robotics. These walking robots always intrigued me and I found them particularly fascinating. Since it is much cooler to build the robot yourself than to simply buy it, I started the project.
The name Lepta or Leptan is an abbreviation of an ant subfamily, the natural inspiration to the walking pattern of Leptan.

If you have any question or would like to contribute feel free to contact me.

Contents
========

.. toctree::
   :maxdepth: 2

   Overview
   Hardware
   Installation
   Code
   status
