Overview
========

Ethymology
----------

Lepta or Leptan is an abbreviation of Leptanillinae, which is a subfamily of ants. `Wikipedia <https://en.wikipedia.org/wiki/Leptanillinae>`_
The walking pattern of Lepta is inspired by the way these ants move.

Hexapods are 6 legged robots, resembling ants in many ways. In addition Lepta/Leptan was quite free when performing web searches or regarding domains, preventing confusions in the future.

Also Lepta is female, because male ants are called Drones and a Hexapod is not a Drone.


How Does it Walk?
-----------------

The controller updates the position of each foot regularly in order to achieve a so called gait.

Gait
~~~~

A gait generally consists of two phases, the swing and the stance phase.
During the swing phase, the foot is in the air, swinging forward.
In the stance phase, the the food is on the ground, moving backwards and pushing the body forward while doing so.

.. figure:: Pictures/Gait_Phases_01.gif
  :align: center
  :figwidth: 80%

  A single leg of Lepta performing a gait with swing and stance phase.

In bipedal creatures one foot is in stance phase while the other is in swing phase. Consider the human, who has always one foot on the ground, while the other is swinging forward.

With multi legged creatures there are more options. A Hexapod can for example have 3 legs in stance while the other 3 are in the swing phase.
In this configuration the left front leg is in stance while the right front leg is in swing and with alternating rows for the subsequent pairs of legs.

This is called a tripod gait and is commonly used by ants and other 6 legged animals.

.. figure:: Pictures/Tripod_Gait_01.gif
  :align: center
  :figwidth: 80%

  Motion principle of Lepta: While three legs are in stance, the three opposing legs are in swing phase. Note how the roles alternate for each pair of legs.


Inverse Kinematics
~~~~~~~~~~~~~~~~~~

Now that we have determined the motion of each foot, the Raspberry Pi has to control each servo in order for every foot to reach its destination.
This is achieved with inverse kinematics: you input the coordinates of a foot and it outputs the required angle for each joint.

These angles are than communicated to each servo.

Construction
-----------------

Hardware
~~~~~~~~~~~~~~~~~~

The brain of Lepta is a Raspberry Pi 4.
18 pcs Dynamixel AX-12A digital Servo motors together with a U2D2 compose the 6 legs. The frame and legs are assembled from 3D printed parts. Power delivery is handled by a 3S LiPo battery through DC/DC converters. A voltage and current meter supervises power delivery. An LED ring serves as status and power indicator.


Software
~~~~~~~~~~~~~~~~~~
The software for Lepta consists of several components with different responsibilities.

Primarily, there is the main code, that translates control inputs to servo commands and controls motion and control of Lepta. This code is writtin in C/C++ and provides the core functionallities of Lepta.

In addition, there is a supervisor, that observes current, voltages, temperatures and signals to the user using the LED ring. The supervisor is written in python with many libraries for parts were readily available.

Control of the robot is done using a joypad.  Currently, I use a XBOX One controller, which can be directly connected to the Raspberry Pi via Bluetooth.

There is also a visualization (RVIZ) which you can run on your PC without the physical robot. A proper simulation (Gazebo) is not yet implemented, but planned for the future.

The full code can be found on `Gitlab. <https://gitlab.com/Combinatrix/project-leptan/>`_
