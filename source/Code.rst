.. _CodeGuide:

Code
====

This section documents the interesting, critical or complicated parts of project leptans code base. Thereby this section is structured in agreement with the code's file structure. Thus looking for the documentation to a specific piece of code just means looking up at the same location as in the directory tree.

.. toctree::
   :maxdepth: 3
   :caption: Contents:

   Code/DynamixelSDK
   Code/Hexapod_Control
   Code/Hexapod_Description
   Code/Hexapod_Msgs
   Code/Launch_Files
   Code/Supervisor
