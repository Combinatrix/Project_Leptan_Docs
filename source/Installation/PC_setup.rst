Visualization Setup
===================

The visualization is intended to test the robot behavior without the actual robot.

.. image:: ../Pictures/Lepta_walking_1.gif
   :align: center

For using the PC visualization a joypad is required. Right now only an XBOX One joypad has been programmed.
Other input device will be implemented as requests arise and hardware for testing is available.

Install ROS
-----------

Install the full Desktop version of `ROS2 Foxy. <https://docs.ros.org/en/foxy/Installation.html>`_

Install Colcon, which is used to build the project and Xacro, which is used for model building.

.. code-block:: bash

  sudo apt install python3-colcon-common-extensions
  sudo apt install ros-foxy-xacro

**Note:** You may need to install further packages, if they are not yet present on your system. Carefully read the console output to install missing packages.

Download and build
------------------

Clone the Repo:

.. code-block:: bash

  git clone --recurse-submodules https://gitlab.com/Combinatrix/project-leptan.git


Navigate into the project directory and build the project:

.. code-block:: bash

  cd project-leptan
  colcon build

Or build the project and run the tests:

.. code-block:: bash

  colcon build && colcon test && colcon test-result --verbose

Usage
-----

Source ROS2 as described in the installation package. Then source the generated files.

.. code-block:: bash

  source /opt/ros/foxy/setup.bash
  source install/setup.bash

Connect the joypad.

Launch the visualization:

.. code-block:: bash

  ros2 launch launch_files hexapod_launch.py


The launch file checks if it is run on a Raspberry or not. According to this either the robot or the visualization is started.


Control
-------

As of time of writing the controls are (`Button guide <https://www.drivers.com/wp-content/uploads/2018/06/xcc-controller-buttons.jpg>`_):

* **Menu** Turn on, go to neutral position
* **A** Stand up
* **B** Sit down
* **X** Switch between walking mode and body rotation mode
* **Joy Stick Left** Walk
* **Joy Stick Right** Turn/Rotate
