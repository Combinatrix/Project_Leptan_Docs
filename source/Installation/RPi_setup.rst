Raspberry Pi Setup
==================

The Raspberry Pi 4 in the center of the assembly is the literal brain of Lepta and runs all the software. This in turn requires the following operations to be executed on the Raspberry Pi itself.

Setup Leptas backbone
---------------------

Install Ubuntu Server 20.04 LTS on Raspberry
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

1. Download the image `ubuntu.com <https://ubuntu.com/download/raspberry-pi>`_
2. `Installation instruction <https://www.raspberrypi.org/documentation/installation/installing-images/>`_

Install ROS2 Foxy
~~~~~~~~~~~~~~~~~

These instructions follow closely the manuals provided by  `roboticsbackend.com <https://roboticsbackend.com/install-ros2-on-raspberry-pi/>`_
or on the `ROS2 homepage. <https://docs.ros.org/en/galactic/Installation/Ubuntu-Install-Debians.html>`_ Refer to these sources in case of trouble.

Setup locale.

.. code-block:: bash

  sudo locale-gen en_US en_US.UTF-8
  sudo update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
  export LANG=en_US.UTF-8

Setup sources.

.. code-block:: bash

  sudo apt update && sudo apt install curl gnupg2 lsb-release
  sudo curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key  -o /usr/share/keyrings/ros-archive-keyring.gpg
  echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/ros2.list > /dev/null

Install ROS2.

.. code-block:: bash

  sudo apt update
  sudo apt install ros-foxy-ros-base

To automatically source the ROS files and the Project Leptan files, add these lines to the end of the .bashrc file.

.. code-block:: bash

  source /opt/ros/foxy/setup.bash
  source /home/ubuntu/project-leptan/Software/install/setup.bash

Finally, we need to install Colcon and the CPP compiler.

.. code-block:: bash

  sudo apt install python3-colcon-common-extensions
  sudo apt install g++

Clone the Git repo
~~~~~~~~~~~~~~~~~~

.. code-block:: bash

  git clone --recurse-submodules https://gitlab.com/Combinatrix/project-leptan.git

For the sake of this installation, we will assume, that you clone project leptan into the home directory of the ubuntu installation, i.e. the project directory is :code:`/home/ubuntu/project-leptan`.


Install Supervisor Requirements
-------------------------------

The supervisor is an independent program which observes and indicates the state of Lepta. It monitors the battery voltage and indicates its status on the LED ring, and governs the bluetooth connections.
It also starts the hexapod Controller as soon as a joypad is connected.

The supervisor is written in Python 3. Install pip, if not yet present, to install further packages for the supervisor.

.. code-block:: bash

  sudo apt install python3-pip

WS2812 LEDs
~~~~~~~~~~~

The LED ring uses WS2812 LEDs. Install the neopixel SPI driver for the LED ring.

.. code-block:: bash

  sudo pip3 install adafruit-circuitpython-neopixel-spi


SPI
~~~

SPI is required to drive the LED ring. Detailed instructions can be found at `SPI <https://forum.up-community.org/discussion/2141/solved-tutorial-gpio-i2c-spi-access-without-root-permissions>`_
and `Neopoixel <https://learn.adafruit.com/circuitpython-neopixels-using-spi>`_.

For SPI-bus access create the file :code:`/etc/udev/rules.d/50-spi.rules` with the following contents.

.. code-block:: bash

  SUBSYSTEM=="spidev", GROUP="spiuser", MODE="0660"

To make your changes take effect, reboot the Raspberry Pi or reload the udev rules.

Next, copy and paste or type these lines into the terminal as the user you want to give access to the SPI bus.

.. code-block:: bash

  sudo groupadd spiuser
  sudo adduser "$USER" spiuser

INA260
~~~~~~

The INA260 is a power meter chip which can measure not only voltage but also current and the according power consumption.
Communication is using I2C.

install the git package directly with this command:

.. code-block:: bash

  pip3 install git+https://github.com/jveitchmichaelis/ina260.git

For I2C-bus access create the file :code:`/etc/udev/rules.d/51-i2c.rules` with the following contents:

.. code-block:: bash

  SUBSYSTEM=="i2c-dev", GROUP="i2cuser", MODE="0660"

Again, reboot the Raspberry Pi or reload the udev rules, to make these changes take effect.

Also grant your user access to the I2C bus.

.. code-block:: bash

  sudo groupadd i2cuser
  sudo adduser "$USER" i2cuser


Temperature
~~~~~~~~~~~

The temperature of the raspberry pi chip is used to regulate the speed of the fan for cooling. This is not strictly required, but a raspberry pi 4 runs quite hot without any additional cooling.

.. code-block:: bash

  pip3 install gpiozero

For GPIO access without root create the file :code:`/etc/udev/rules.d/52-gpio.rules` with the following contents

.. code-block:: bash

  SUBSYSTEM=="bcm2835-gpiomem", GROUP="gpio", MODE="0660"
  SUBSYSTEM=="gpio", GROUP="gpio", MODE="0660"
  SUBSYSTEM=="gpio*", PROGRAM="/bin/sh -c '\
          chown -R root:gpio /sys/class/gpio && chmod -R 770 /sys/class/gpio;\
          chown -R root:gpio /sys/devices/virtual/gpio && chmod -R 770 /sys/devices/virtual/gpio;\
          chown -R root:gpio /sys$devpath && chmod -R 770 /sys$devpath\'"

Again, grant access to the GPIO interface to your user.

.. code-block:: bash

  sudo groupadd gpio
  sudo adduser "$USER" gpio


Enable shutdown
~~~~~~~~~~~~~~~
.. code-block:: bash

  sudo chmod u+s /sbin/shutdown


Enable Linux event interface in Python
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. code-block:: bash

  pip3 install evdev


Setup Joypad
------------

Instruction can be found `pimylifeup.com <https://pimylifeup.com/xbox-controllers-raspberry-pi/>`_.

Install required Software for XBOX controller.

.. code-block:: bash

  sudo apt install xboxdrv

This command disables the Enhanced Re-Transmission Mode (ERTM) of the Bluetooth module. With it enabled, the Xbox Controller won’t pair correctly.

.. code-block:: bash

  echo 'options bluetooth disable_ertm=Y' | sudo tee -a /etc/modprobe.d/bluetooth.conf


Install Bluetooth
~~~~~~~~~~~~~~~~~
.. code-block:: bash

  sudo apt install pi-bluetooth

Follow a guide like `this one <https://pimylifeup.com/xbox-controllers-raspberry-pi/>`_ in order to
pair & connect the controller.

Add the user to the input group in order to access the joypad:

.. code-block:: bash

  sudo adduser "$USER" input


Further Requirements
~~~~~~~~~~~~~~~~~~~~

.. code-block:: bash

  sudo apt install ros-foxy-joy


Run the Supervisor
------------------

The following command starts the supervisor:

.. code-block:: bash

  ros2 run supervisor supervisor_node

Start at boot
~~~~~~~~~~~~~

Only thing left to do is to auto-start the supervisor at boot using Crontab.

Open the crontab file:

.. code-block:: bash

  crontab -e

Add this line:

.. code-block:: bash

  @reboot bash -ic "ros2 run supervisor supervisor_node"
